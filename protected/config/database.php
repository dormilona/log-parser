<?php

// This is the database connection configuration.
return array(
    'connectionString' => 'mysql:host=localhost;dbname=log_parser',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
);