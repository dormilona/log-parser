<?php

class ApiController extends Controller
{
    public function actionList() 
    {
        if (API_AUTH === true) {
            $this->_checkAuth();
        }
        
        switch ($_GET['model']) {
            case 'log':
                $attributes = [];
                
                if (Yii::app()->request->getParam('ip')) {
                    $attributes['ip'] = Yii::app()->request->getParam('ip');                    
                }
                
                if (Yii::app()->request->getParam('date_from')) {
                    $attributes['date_from'] = Yii::app()->request->getParam('date_from');                    
                }
                
                if (Yii::app()->request->getParam('date_to')) {
                    $attributes['date_to'] = Yii::app()->request->getParam('date_to');                    
                }
                
                $sort = Yii::app()->request->getParam('sort', ['access_time' => 'DESC']); 
                $keys = array_keys($sort);
                $firstKey = $keys[0];
                $values = array_values($sort);
                $firstValue = $values[0];
                
                $model = new Logs('search');
                $model->unsetAttributes();
                
                if (!empty($attributes)) {
                    $model->attributes = $attributes;
                }
                
                $models = $model->search(false, $firstKey . ' ' . $firstValue)->getData();
                break;
            default:
                $this->_sendResponse(501, sprintf(
                    'Error: Метод <b>list</b> не найден в модели <b>%s</b>',
                    $_GET['model']));
                Yii::app()->end();
        }
 
        if (empty($models)) {
            $this->_sendResponse(200, 
                sprintf('Модель <b>%s</b> не найдена', $_GET['model']) );
        } else {
            $rows = array();
           
            foreach($models as $model) {
                $rows[] = $model->attributes;
            }
           
            $this->_sendResponse(200, CJSON::encode($rows));
        }
    }
    
    private function _checkAuth()
    {
        if (!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
            $this->_sendResponse(401);
        }
        
        $username = $_SERVER['HTTP_X_USERNAME'];
        $password = $_SERVER['HTTP_X_PASSWORD'];
        $user = User::model()->find('LOWER(email)=?', array(strtolower($username)));
        
        if ($user === null) {
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if (!$user->validatePassword($password)) {
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        header('HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status));
        header('Content-type: ' . $content_type);

        if ($body !== '') {
            echo $body;
        } else {
            $message = '';
            
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>';

            echo $body;
        }
        Yii::app()->end();
    }
 
    private function _getStatusCodeMessage($status) 
    {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
       );
        
       return (isset($codes[$status])) ? $codes[$status] : '';
    }
}
