<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

if (Yii::app()->user->isGuest) {
?>

Для просмотра логов авторизуйтесь на сайте: <a href="/site/login">Авторизация</a>

<?php } else { ?>

    <a href="/" class="btn btn-primary">Сбросить фильтр</a>
    
    <a href="#" class="btn btn-primary" id="parse-button">Парсинг лог-файла</a>
    
    <div id="loader"><span>Загрузка...</span></div>
    
<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'logs-grid',
        'dataProvider' => $dataProvider,
        'filter' => $model,
        'afterAjaxUpdate'=>"function(id, data) {
            jQuery('#date_from').datepicker(jQuery.datepicker.regional['ru'], {'dateFormat':'dd.mm.yy','language':'ru'});
            jQuery('#date_to').datepicker(jQuery.datepicker.regional['ru'], {'dateFormat':'dd.mm.yy','language':'ru'});
        }",
        'columns' => array(
            'ip',
            'request',
            'status',
            array(
                'name' => 'access_time',
                'filter'=>
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model,
                        'language' => Yii::app()->getLanguage(),
                        'attribute' => 'date_from',
                        'mode' => 'date',
                        'htmlOptions'=>array('class' => 'input date', 'placeholder' => 'с', 'id' => 'date_from', 'value' => $_GET['Logs']['date_from']),
                        'options' => array(
                            'dateFormat' => 'dd.mm.yy',
                        )
                    ),  true) . 
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model,
                        'language' => Yii::app()->getLanguage(),
                        'attribute' => 'date_to',
                        'mode' => 'date',
                        'htmlOptions' => array('class' => 'input date', 'placeholder' => 'по', 'id' => 'date_to', 'value' => $_GET['Logs']['date_to']),
                        'options' => array(
                            'dateFormat' => 'dd.mm.yy',
                        )
                    ),  true) 
                ,
                'value' => 'date("Y-m-d H:i", strtotime($data->access_time))',
                'htmlOptions' => array('style' => 'width:400px;')
            ),
    ),
    ));
} 
?>
