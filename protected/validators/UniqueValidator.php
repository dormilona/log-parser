<?php

class UniqueValidator extends CValidator
{
    public $attributeNames; // массив аттрибутов, которые необходимо валидировать вместе как "комбинированный ключ"
        

    protected function validateAttribute($object, $attribute)
    {
        $criteria = new CDbCriteria();
        
        foreach ($this->attributeNames as $name) {
            $criteria->addSearchCondition($name, $object->$name, false);
        }

        if ($object->exists($criteria) ) {
            $this->addError($object, $attribute, $object->$attribute . ' has already been taken.');
        }
    }
}
