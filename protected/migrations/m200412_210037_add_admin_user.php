<?php
     
class m200412_210037_add_admin_user extends CDbMigration
{
    public function up()
    {
        $sql = "INSERT INTO `user` (
            `email`,
            `password`,
            `name`,
            `date`,
            `status`,
            `role` 
         ) VALUES (
            'admin@myproject.ru',
            '" . md5(123456) . "',
            'Administrator',
            NOW(),
            1,
            'admin'
         );";
        Yii::app()->db->createCommand($sql)->execute();
    }
     
    public function down()
    {
        $sql = "DELETE FROM `user` WHERE `email` = 'admin@myproject.ru'";
        Yii::app()->db->createCommand($sql)->execute();
    }
}
