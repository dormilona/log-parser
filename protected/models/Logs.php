<?php

/**
 * This is the model class for table "logs".
 *
 * The followings are the available columns in table 'logs':
 * @property integer $id
 * @property string $ip
 * @property string $access_time
 * @property string $request
 * @property integer $status
 */
class Logs extends CActiveRecord
{
        public $date_from;
        public $date_to;
        
        public function primaryKey()
        {
            return 'id';
        }

        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ip, access_time, request, status', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>15),
			array('request', 'length', 'max'=>255),
                        array('ip', 'uniqueValidator', 'attributeNames' => array(
                            'ip', 'access_time', 'request', 'status')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ip, date_to, date_from, request, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ip' => 'IP',
			'access_time' => 'Дата/время',
			'request' => 'Запрос',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($pagination = 15, $default_sort = 'access_time DESC')
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('ip', $this->ip, true);
		$criteria->compare('request', $this->request, true);
		$criteria->compare('status', $this->status);
                
                $to = $this->date_to ?
                    (new \DateTime($this->date_to, new \DateTimeZone('Europe/Moscow')))->format("Y-m-d 23:59:59") :
                    (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format("Y-m-d 23:59:59");
                
                $from = $this->date_from ?
                    (new \DateTime($this->date_from, new \DateTimeZone('Europe/Moscow')))->format("Y-m-d 00:00:00") :
                    (new \DateTime('01.01.1970', new \DateTimeZone('Europe/Moscow')))->format("Y-m-d 00:00:00");
                
                $criteria->addBetweenCondition('access_time', $from, $to);

		return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => $pagination !== false ? 
                        array('pageSize' => $pagination) : $pagination,
                    'sort' => array(
                        'defaultOrder' => $default_sort
                    )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Logs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
